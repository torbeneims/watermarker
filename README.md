## Install OpenCV
```
pip install opencv-python
```


## Arguments

The script accepts the following arguments:

* `--folder`: Flag indicating to process a folder instead of individual input image files. If specified, the script will treat the provided input files as a folder path and process all images within that folder.

* `--overlay_path <path>` (required): Path to the overlay image. The overlay image will be applied on top of the input images. This argument is required.

* `--start_index <index>` (default: 1): Starting index for the output images. Each output image will be named as `<index>_<original_name>.<extension>`, where `<index>` is incremented for each input image.

* `--opacity <value>` (default: 0.5): Opacity of the overlay image. The value should be between 0.0 and 1.0, where 0.0 means fully transparent and 1.0 means fully opaque.

* `--font_scale <value>` (default: 1): Scale factor for the font size used to display the number on the output image. Increase this value to make the number larger.

* `--text_thickness <value>` (default: 1): Thickness of the text in the output image.

* `--border_thickness <value>` (default: 1): Thickness of the border around the text in the output image.

* `--text_color <R,G,B>`: Color of the text in the output image specified as three comma-separated values for the red, green, and blue channels. Example: `255,0,0` for red.

* `--border_color <R,G,B>`: Color of the border around the text in the output image specified as three comma-separated values for the red, green, and blue channels. Example: `0,0,255` for blue.

* `input_files...`: List of input image files or a folder path containing the input images. If the `--folder` flag is used, provide the path to the folder. Otherwise, specify individual input image files.

## Examples

Apply the overlay image to individual input files:

python image_overlay.py --overlay_path overlay.png image1.jpg image2.jpg image3.jpg


Specify additional options such as starting index, opacity, font scale, and colors:
python image_overlay.py --overlay_path overlay.png --start_index 10 --opacity 0.8 --font_scale 1.5 --text_color 255,0,0 --border_color 0,0,255 input_files...


# DE
## Argumente
Das Skript akzeptiert die folgenden Argumente:

* `--folder`: Flag, das angibt, dass ein Ordner anstelle einzelner Eingabebilddateien verarbeitet werden soll. Wenn angegeben, behandelt das Skript die bereitgestellten Eingabedateien als Pfad zu einem Ordner und verarbeitet alle Bilder in diesem Ordner.

* `--overlay_path <path>` (erforderlich): Pfad zum Überlagerungsbild. Das Überlagerungsbild wird auf die Eingabebilder angewendet. Dieses Argument ist erforderlich.

* `--start_index <index>` (Standard: 1): Startindex für die Ausgabebilder. Jedes Ausgabebild wird im Format <index>_<original_name>.<extension> benannt, wobei <index> für jedes Eingabebild erhöht wird.

* `--opacity <value>` (Standard: 0.5): Deckkraft des Überlagerungsbildes. Der Wert sollte zwischen 0,0 und 1,0 liegen, wobei 0,0 vollständig transparent und 1,0 vollständig undurchsichtig bedeutet.

* `--font_scale <value>` (Standard: 1): Skalierungsfaktor für die Schriftgröße, die die Nummer auf dem Ausgabebild darstellt. Erhöhen Sie diesen Wert, um die Nummer größer zu machen.

* `--text_thickness <value>` (Standard: 1): Dicke des Textes im Ausgabebild.

* `--border_thickness <value>` (Standard: 1): Dicke des Rahmens um den Text im Ausgabebild.

* `--text_color <R,G,B>`: Farbe des Textes im Ausgabebild, angegeben als drei durch Kommas getrennte Werte für die Rot-, Grün- und Blaukanäle. Beispiel: 255,0,0 für Rot.

* `--border_color <R,G,B>`: Farbe des Rahmens um den Text im Ausgabebild, angegeben als drei durch Kommas getrennte Werte für die Rot-, Grün- und Blaukanäle. Beispiel: 0,0,255 für Blau.

* `input_files...`: Liste der Eingabebilddateien oder Pfad zu einem Ordner, der die Eingabebilder enthält. Wenn das --folder-Flag verwendet wird, geben Sie den Pfad zum Ordner an. Andernfalls geben Sie einzelne Eingabebilddateien an.

## Beispiele
Überlagerung des Überlagerungsbildes auf einzelne Eingabedateien:

```py
python image_overlay.py --overlay_path overlay.png image1.jpg image2.jpg image3.jpg
```
Angabe zusätzlicher Optionen wie Startindex, Deckkraft, Skalierungsfaktor für die Schriftgröße und Farben:

```py
python image_overlay.py --overlay_path overlay.png --start_index 10 --opacity 0.8 --font_scale 1.5 --text_color 255,0,0 --border_color 0,0,255 input_files...
```

# PL
## Argumenty
Skrypt akceptuje następujące argumenty:

--folder: Flaga wskazująca przetwarzanie folderu zamiast pojedynczych plików obrazów. Jeśli jest określona, skrypt traktuje podane pliki wejściowe jako ścieżkę do folderu i przetwarza wszystkie obrazy w tym folderze.

--overlay_path <path> (wymagane): Ścieżka do obrazu nakładki. Obraz nakładki zostanie nałożony na obrazy wejściowe. Ten argument jest wymagany.

--start_index <index> (domyślnie: 1): Początkowy indeks dla obrazów wyjściowych. Każdy obraz wyjściowy będzie nazwany jako <index>_<oryginalna_nazwa>.<rozszerzenie>, gdzie <index> jest inkrementowany dla każdego obrazu wejściowego.

--opacity <value> (domyślnie: 0.5): Przezroczystość obrazu nakładki. Wartość powinna być między 0,0 a 1,0, gdzie 0,0 oznacza całkowitą przezroczystość, a 1,0 oznacza całkowitą nieprzezroczystość.

--font_scale <value> (domyślnie: 1): Współczynnik skalowania rozmiaru czcionki używanej do wyświetlania liczby na obrazie wyjściowym. Zwiększ tę wartość, aby liczba była większa.

--text_thickness <value> (domyślnie: 1): Grubość tekstu na obrazie wyjściowym.

--border_thickness <value> (domyślnie: 1): Grubość obramowania tekstu na obrazie wyjściowym.

--text_color <R,G,B>: Kolor tekstu na obrazie wyjściowym podany jako trzy wartości oddzielone przecinkami dla kanałów czerwonego, zielonego i niebieskiego. Przykład: 255,0,0 dla czerwieni.

--border_color <R,G,B>: Kolor obramowania tekstu na obrazie wyjściowym podany jako trzy wartości oddzielone przecinkami dla kanałów czerwonego, zielonego i niebieskiego. Przykład: 0,0,255 dla niebieskiego.

input_files...: Lista pojedynczych plików obrazów lub ścieżka do folderu zawierającego obrazy wejściowe. Jeśli używana jest flaga --folder, podaj ścieżkę do folderu. W przeciwnym razie podaj pojedyncze pliki obrazów wejściowych.

Przykłady
Nakładanie obrazu nakładki na pojedyncze pliki wejściowe:
```py
python image_overlay.py --overlay_path overlay.png image1.jpg image2.jpg image3.jpg
```

Określanie dodatkowych opcji, takich jak początkowy indeks, przezroczystość, współczynnik skalowania czcionki i kolory:
```py
python image_overlay.py --overlay_path overlay.png --start_index 10 --opacity 0.8 --font_scale 1.5 --text_color 255,0,0 --border_color 0,0,255 input_files...
```

# NO
## Argumenter
Skriptet aksepterer følgende argumenter:

--folder: Flagg som indikerer at en mappe skal behandles i stedet for individuelle bildefiler. Hvis dette angis, vil skriptet behandle den angitte inputmappen og prosessere alle bildene i mappen.

--overlay_path <sti> (påkrevd): Sti til overlay-bildet. Overlay-bildet blir påført på toppen av inngangsbildene. Dette argumentet er påkrevd.

--start_index <indeks> (standard: 1): Startindeks for utdata-bildene. Hvert utdata-bilde vil bli navngitt som <indeks>_<opprinnelig_navn>.<utvidelse>, der <indeks> økes for hvert inngangsbilde.

--opacity <verdi> (standard: 0.5): Gjennomsiktighet for overlay-bildet. Verdien bør være mellom 0,0 og 1,0, der 0,0 betyr fullstendig gjennomsiktig og 1,0 betyr fullstendig ugjennomsiktig.

--font_scale <verdi> (standard: 1): Skaleringsfaktor for skriftstørrelsen som brukes for å vise nummeret på utdata-bildet. Øk denne verdien for å gjøre nummeret større.

--text_thickness <verdi> (standard: 1): Tykkelse på teksten i utdata-bildet.

--border_thickness <verdi> (standard: 1): Tykkelse på rammen rundt teksten i utdata-bildet.

--text_color <R,G,B>: Farge på teksten i utdata-bildet angitt som tre komma-separerte verdier for rød, grønn og blå kanal. Eksempel: 255,0,0 for rød.

--border_color <R,G,B>: Farge på rammen rundt teksten i utdata-bildet angitt som tre komma-separerte verdier for rød, grønn og blå kanal. Eksempel: 0,0,255 for blå.

input_files...: Liste over inngangsbildefiler eller en mappensti som inneholder inngangsbildene. Hvis --folder-flagget brukes, oppgi mappenstien. Ellers angis individuelle inngangsbildefiler.

## Eksempler
Påfør overlay-bildet på individuelle inngangsbilder:
```py
python image_overlay.py --overlay_path overlay.png image1.jpg image2.jpg image3.jpg
```

Angi tilleggsalternativer som startindeks, gjennomsiktighet, skaleringsfaktor for skriftstørrelse og farger:
```py
python image_overlay.py --overlay_path overlay.png --start_index 10 --opacity 0.8 --font_scale 1.5 --text_color 255,0,0 --border_color 0,0,255 input_files...
```
