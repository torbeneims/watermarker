import cv2
import os
import argparse
import glob

def tuple_default(a, b):
    if a is None:
        a = (None,) * len(b)
    a = list(a)
    for i in range(min(len(b), len(a))):
        if a[i] is None:
            a[i] = b[i]
    return tuple(a)

def overlay_image(input_path, overlay_path, output_path, number, opacity=0.5, font_scale=1, text_thickness=1,
                  border_thickness=1, text_color=None, border_color=None):
   # Read the input image and overlay image
    image = cv2.imread(input_path)
    overlay = cv2.imread(overlay_path)

    # Check if the images were loaded successfully
    if image is None:
        print(f"Failed to load image from '{input_path}'")
        return
    if overlay is None:
        print(f"Failed to load overlay image from '{overlay_path}'")
        return

    # Resize the overlay image to match the input image dimensions
    overlay = cv2.resize(overlay, (image.shape[1], image.shape[0]))

    # Blend the overlay image with the input image
    opacity = 0.5
    blended = cv2.addWeighted(image, 1, overlay, 1 - opacity, 0)

    # Add the number to the bottom right corner with a black border
    font = cv2.FONT_HERSHEY_DUPLEX

    # Scaling correction
    font_scale *= 5
    text_thickness = int(text_thickness * font_scale * 4 / 3)
    border_thickness = int(text_thickness * font_scale / 2)

    text_color = tuple_default(text_color, (0, 0, 0))[::1]
    border_color = tuple_default(border_color, (255, 255, 255))[::-1]
    text_size, _ = cv2.getTextSize(f"{number}", font, font_scale, text_thickness)
    text_x = int(image.shape[1] - text_size[0] * 1.1)
    text_y = int(image.shape[0] - text_size[1] * 0.5)

    # Add the black border to the text
    cv2.putText(blended, f"{number}", (text_x, text_y), font, font_scale, border_color, text_thickness + border_thickness)

    # Add the number on top of the black border
    cv2.putText(blended, f"{number}", (text_x, text_y), font, font_scale, text_color, text_thickness)

    # Save the resulting image to the output path
    cv2.imwrite(output_path, blended)
    print(f"Image with overlay and number saved successfully to '{output_path}'")


def parse_color_string(color_string):
    if color_string is None:
        return None
    try:
        color_values = color_string.split(",")
        color_values = [int(value) for value in color_values]
        if len(color_values) != 3:
            raise ValueError
        return tuple(color_values)
    except ValueError:
        print("Invalid color format. Expected format: R,G,B")
        exit(1)


def parse_cli_arguments():
    parser = argparse.ArgumentParser(description="Apply image overlay with number")
    parser.add_argument("--folder", action="store_true", help="Path to the folder containing input image files")
    parser.add_argument("--overlay_path", help="Path to the overlay image")
    parser.add_argument("input_files", nargs="+", help="List of input image files")
    parser.add_argument("--start_index", type=int, default=1, help="Starting index for the output images")
    parser.add_argument("--opacity", type=float, default=0.5, help="Opacity of the overlay image (0.0-1.0)")
    parser.add_argument("--font_scale", type=float, default=1, help="Scale factor for the font size")
    parser.add_argument("--text_thickness", type=int, default=1, help="Thickness of the text")
    parser.add_argument("--border_thickness", type=int, default=1, help="Thickness of the border")
    parser.add_argument("--text_color", type=str, default=None, help="Color of the text (R,G,B)")
    parser.add_argument("--border_color", type=str, default=None, help="Color of the border (R,G,B)")
    return parser.parse_args()

# Example usage
if __name__ == "__main__":
    args = parse_cli_arguments()

    if args.folder:
        # Process images in a folder
        input_files = glob.glob(os.path.join(args.input_files[0], "*.*"))  # Adjust the file extension if necessary
    else:
        # Process individual input files
        input_files = args.input_files

    # Iterate over the input files and apply the overlay_image function
    for i, input_file in enumerate(input_files):
        input_path = input_file
        output_file = f"{i+1}_{os.path.basename(input_file)}"  # Prepend the index to the output image name
        output_path = os.path.join("out", output_file)

        overlay_image(input_path, args.overlay_path, output_path, args.start_index+i,
                      opacity=args.opacity, font_scale=args.font_scale,
                      text_thickness=args.text_thickness, border_thickness=args.border_thickness,
                      text_color=parse_color_string(args.text_color), border_color=parse_color_string(args.border_color))
